/**
 * Critial Impact
 *
 * @author  J.M Field
 */

 /**
  * Critical Impact Class
  * 
  * @constructor
  * @param {Object} reqFields 
  */
 function CriticalImpact(reqFields) {
  this.reqFields = reqFields;
  this.whitespace_CI = " \t\n\r";
  this.userLang = (function() {
    var userLang = (navigator.language) ? navigator.language : navigator.userLanguage;

    userLang = userLang.substr(0,2);
    userLang = userLang.toUpperCase();

    return userLang;
  })();
}

/**
 * @method isValidEmail_CI()
 * Validate email input.
 * 
 * Email address must be of form a@b.c ... in other words:
 * there must be at least one character before the @
 * there must be at least one character before and after the .
 * the characters @ and . are both required
 * 
 * @param {String} email
 */
CriticalImpact.prototype.isValidEmail_CI = function(email) {
  if (this.isEmpty_CI(email)) return false;
  
  if (this.isWhitespace_CI(email)) return false;
      
  // there must be >= 1 character before @, so we
  // start looking at character position 1 
  // (i.e. second character)
  var i = 1;
  var emailLength = email.length;

  // look for @
  while ((i < emailLength) && (email.charAt(i) != "@")) { i++; }

  if ((i >= emailLength) || (email.charAt(i) != "@")) return false;
  else i += 2;

  while ((i < emailLength) && (email.charAt(i) != ".")) { i++; }

  // there must be at least one character after the .
  if ((i >= emailLength - 1) || (email.charAt(i) != ".")) return false;
  else return true;
};

/**
 * Validate phone number.
 * 
 * @param {String} phone 
 */
CriticalImpact.prototype.isValidPhone_CI = function(phone) {
  if (this.isEmpty_CI(phone)) return false;
    
  if (this.isWhitespace_CI(phone)) return false;
      
  // there must be at least one character after the .
  var regexObj = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

  if (regexObj.test(phone)) {
      return true;
      //var formattedPhoneNumber = subjectString.replace(regexObj, "($1) $2-$3");
  } else {
      return false;
  }
};

/**
 * Validate zip code.
 * 
 * @param {String} zip 
 */
CriticalImpact.prototype.isValidZipCode_CI = function(zip) {
  if (this.isEmpty_CI(zip)) return false;
    
  if (this.isWhitespace_CI(zip)) return false;
      
  // there must be at least one character after the .
  var regexObj = /(^\d{5}$)|(^\d{5}-\d{4}$)/;

  if (regexObj.test(zip)) {
      return true;
      //var formattedPhoneNumber = subjectString.replace(regexObj, "($1) $2-$3");
  } else {
      return false;
  }
};

/**
 * Check if argument is a number. 
 * 
 * @param {Number} number  
 */
CriticalImpact.prototype.isNumeric_CI = function(number) {
  if (this.isEmpty_CI(number)) return false;
    
  if (this.isWhitespace_CI(number)) return false;

  return !isNaN(parseFloat(number)) && isFinite(number);
};

/**
 * Check whether string is empty.
 * 
 * @param {String} str 
 */
CriticalImpact.prototype.isEmpty_CI = function(str) {
  return ((str == null) || (str.length == 0));
};

/**
 * Returns true if string s is empty or whitespace characters only.
 * 
 * @param {String} str
 */
CriticalImpact.prototype.isWhitespace_CI = function(str) {
  var i;

  if (this.isEmpty_CI(str)) return true;

  // Search through string characters one by one
  // until we find a non-whitespace character.
  // When we do, return false; if we dont, return true.
  for (i = 0; i < str.length; i++){   
      // Check that current character isnt whitespace.
      var c = str.charAt(i);

      if (this.whitespace_CI.indexOf(c) == -1) return false;
  }
  
  return true;
};

/**
 * Validate form
 * 
 * @param {String} form
 */
CriticalImpact.prototype.checkForm_CI = function(form) {
  var message = "";

  console.log(document[form]);
  
  //if (!isValidEmail_CI(document.CI_subscribeForm.elements[''EmailAddress''].value)) {
  //	document.CI_subscribeForm.elements[''EmailAddress''].style.backgroundColor="yellow";
  //		//alert("Please enter a valid Email Address. (yourname@domain.com)");
  //		message = "Please enter a valid Email Address. (yourname@domain.com)\n\r" ;
  //		document.CI_subscribeForm.elements[''EmailAddress''].focus();
  //		
  //	}
  
  var i = 0;
   
  if (this.reqFields.rfnames) {
    for (i; i < this.reqFields.rfnames.length; i++) { 
      if (document[form].elements[this.reqFields.rfnames[i]].value == "") {
        document[form].elements[this.reqFields.rfnames[i]].style.backgroundColor = "yellow";
        message = message + this.reqFields.rfdesc[i] + " is required \n\r";
      }
    }
  }

  if (this.reqFields.val_email_names) {
    for (i; i < this.reqFields.val_email_names.length; i++) { 
      if (document[form].elements[this.reqFields.val_email_names[i]].value != "" && !this.isValidEmail_CI(document[form].elements[this.reqFields.val_email_names[i]].value)) {
        document[form].elements[this.reqFields.val_email_names[i]].style.backgroundColor = "yellow";
        // alert("Please enter a valid Email Address. (yourname@domain.com)");
        message = message + this.reqFields.val_email_desc[i] + " must be a valid email address. (yourname@domain.com)\n\r" ;
        //document[form].elements[this.reqFields.val_email_names[i]].focus();
      }
    }
  }

  if (this.reqFields.val_phone_names) {
    for (i; i < this.reqFields.val_phone_names.length; i++) { 
      if (document[form].elements[this.reqFields.val_phone_names[i]].value != "" && !this.isValidPhone_CI(document[form].elements[this.reqFields.val_phone_names[i]].value)) {
        document[form].elements[this.reqFields.val_phone_names[i]].style.backgroundColor = "yellow";
        // alert("Please enter a valid Email Address. (yourname@domain.com)");
        message = message + this.reqFields.val_phone_desc[i] + " must be a valid phone number \n\r";        
      }
    }
  }

  if (this.reqFields.val_zipcode_names) {
    for(i; i < this.reqFields.val_zipcode_names.length; i++) { 
      if (document[form].elements[this.reqFields.val_zipcode_names[i]].value != "" && !this.isValidZipCode_CI(document[form].elements[this.reqFields.val_zipcode_names[i]].value)) {
        document[form].elements[this.reqFields.val_zipcode_names[i]].style.backgroundColor = "yellow";
        // alert("Please enter a valid Email Address. (yourname@domain.com)");
        message = message + this.reqFields.val_zipcode_desc[i] + " must be a valid zipcode \n\r";
      }
    }
  }

  if (this.reqFields.val_numeric_names) {
    for(i; i < this.reqFields.val_numeric_names.length; i++) { 
      if (document[form].elements[this.reqFields.val_numeric_names[i]].value != "" && !this.isNumeric_CI(document[form].elements[this.reqFields.val_numeric_names[i]].value)) {
        document[form].elements[this.reqFields.val_numeric_names[i]].style.backgroundColor = "yellow";
        // alert("Please enter a valid Email Address. (yourname@domain.com)");
        message = message + this.reqFields.val_numeric_desc[i] + " must be numeric \n\r";
      }
    }
  }
  /* check submit code: show only if the checkbox is added*/
  /*end check submit code */  

  if (message.length > 1) { 
    alert(message);

    return false;
  } else {
    document[form].submit();
  }
};

/**
 * Check language settings
 */
CriticalImpact.prototype.checkLanguageSetting = function() {
  var confirmation_text_language = "";
  // var default_confirmation_text = "I agree that the above information is correct";
  // var submitConfirmHTML = form_label_language = list_label_language ='';
  var default_btn_text = "Subscribe!";

  console.log(this.userLang);
  
  var form_label_language,
    	list_label_language,
    	btn_text_language;

  // Check for designated languages
  if (this.userLang == 'FR'){
    confirmation_text_language = "";
    form_label_language = "Vous pouvez vous abonner à notre liste d\'envoi en remplissant le formulaire ci-dessous et cliquez sur \"Abonnez-vous\" bouton.";
    list_label_language = "S\'il vous plaît sélectionner vos listes d\'intérêt";
    btn_text_language = "Abonnez-vous";
  } else if (this.userLang == 'DU'){
    confirmation_text_language = "";
    form_label_language = "Sie können auf unsere Mailingliste, indem Sie das folgende Formular aus und klicken Sie auf die Schaltfläche \"Abonnieren\" abonnieren.";
    list_label_language = "Bitte wahlen Sie Ihre Listen von Interesse";
    btn_text_language = "Abonnieren";
  } else if (this.userLang == 'ES'){
    confirmation_text_language = "";
    form_label_language = "Usted puede suscribirse a nuestra lista de correo rellenando el siguiente formulario y haga clic en el botón \"Suscribirse\".";
    list_label_language = "Por favor, seleccione las listas de interés";
    btn_text_language = "Suscribirse";
  } else if (this.userLang == 'IT'){
    confirmation_text_language = "";	
    form_label_language = "È possibile iscriversi alla nostra mailing list compilando il modulo qui sotto e cliccando sul tasto \"Iscriviti\".";	
    list_label_language = "Si prega di selezionare le vostre liste di interesse";
    btn_text_language = "Iscriviti";

  } else { // English
    confirmation_text_language = "";
    form_label_language = "You can subscribe to our mailing list by filling out the form below and clicking on the \"Subscribe!\" button.";
    list_label_language = "Please select your lists of interest";
    btn_text_language = "Subscribe!";
  }

  // Use the browser's inbuilt HTML escape functionalityzto handle many of the characters
  var l_html_div = document.createElement("div");
  l_html_div.innerText = l_html_div.textContent = confirmation_text_language;
  confirmation_text_language = l_html_div.innerHTML;

  // Change the form description/label to the correct language 
  // formLabelHTML = form_label_language;

  // document.getElementById('form_label_display').innerHTML = formLabelHTML;

  // Change the button text to the correct language 
  if (btn_text_language == ''){
    btn_text_language = default_btn_text;	
  }

  document.getElementById('CI_submit').value = btn_text_language;

  // Change the checkbox submit confirmation text to the correct language 
  // submitConfirmHTML = '<input onClick="agreesubmit_CI(this)" type="checkbox" name="submit_confirmation" id="submit_confirmation" value="submit_confirmation">&nbsp;'+confirmation_text_language+'';
};

/**
 * Get parameter value
 * 
 * @param {String} param
 */
CriticalImpact.prototype.my_getParamVal = function(param) {
  var query = location.search;
  var regexp = new RegExp("[\\?&]" + param + "\=([^\&#]*)", "i");
  var val = query.match(regexp) || "";
  
  return val[1] || "";
};

if (
  document.forms.CI_subscribeForm !== undefined && 
  document.forms.CI_subscribeForm.CI_email !== undefined
) {
  var t_email = this.my_getParamVal('myemail');

  if (
    t_email !== undefined && 
    typeof t_email === 'string'&& 
    t_email.length > 5
  ) {
    document.forms.CI_subscribeForm.CI_email.value = t_email;
  }
}


