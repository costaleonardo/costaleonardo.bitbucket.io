/**
 * Critical Impact Forms Init
 */

// window.onload = function() {
//   newsletterForm.checkLanguageSetting();
// };

 /**
 * 01. Coupon Form
 */

var couponForm = new CriticalImpact({
  rfnames:         new Array("CI_email","CI_firstname"),
  rfdesc:          new Array("Email Address","First Name"), 
  val_email_names: new Array("CI_email"), 
  val_email_desc:  new Array("Email Address")
});

document.getElementById('CI_couponFormSubmit').addEventListener('click', function() {
  couponForm.checkForm_CI('CI_couponForm');
});

 /**
 * 02. Coupon form #2
 */

var couponFormTwo = new CriticalImpact({
  rfnames:         new Array("CI_email","CI_firstname"),
  rfdesc:          new Array("Email Address","First Name"), 
  val_email_names: new Array("CI_email"), 
  val_email_desc:  new Array("Email Address")
});

document.getElementById('CI_couponFormTwoSubmit').addEventListener('click', function() {
  couponFormTwo.checkForm_CI('CI_couponFormTwo');
});


